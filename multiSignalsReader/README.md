
#Multi Signals Reader


Request, readn and storage the serial reads of the arduino

##Parts


###Arduino (mulmultiSignalsReader.ino):


Program to read the pins of the Arduino in a dynamic way, messages request and response are expressed in the format of [json].


###Python (mulmultiSignalsReader.ino):


Program that interact with the serial port, request data using [json] format and store the response data in a file.


##Requeriments

- [Python 2.7] - Interpreted lenguaje
- [Json Parser] - Arduino library


##Instrutions

- Compile and upload "mulmultiSignalsReader.ino" on a Arduino
- (Optional) Change the pins to read and put titles on "serialReader.py"
- Run "serialReader.py", example in the shell:
    
        python serialReader
- The program can be stopped using ctlr + c

##License
MIT

[json]:http://json.org/
[Json Parser]:https://github.com/udp/json-parser
[Python 2.7]:https://www.python.org/downloads/

