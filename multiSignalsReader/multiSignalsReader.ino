//   {"data":[1,2,4,3],"time":101}
#include <JsonParser.h>

using namespace ArduinoJson::Parser;

#define DEBUG 1
#define MIN_TIME 100
//Set the numer of inputs to use, the arduino mega have 15
#define MIN_ANALOG_IN 0
#define MAX_ANALOG_IN 15

struct info_read{
  int time = MIN_TIME;
  int sizeArrInputs = 0;
  int * arrInputs = NULL;
  int * arrData = NULL;
  byte isRead = 0;
} info;

unsigned long initTime;

void setup()
{
    Serial.begin(9600);
    //Set the range of analog pins on input mode
    setModeAnalogPin(MIN_ANALOG_IN, MAX_ANALOG_IN, INPUT);
}

void loop()
{
  char inputString[1500]="";
  if(Serial.available()>0)
  {
    Serial.readBytesUntil('\n', inputString, 1500);
    Serial.flush();
    if(DEBUG){
      Serial.print("Input string: ");
      Serial.println(inputString);
    }
    jsonToArrayInt(inputString);
  }
  if(info.isRead){
    readAnalogInputs(info.arrInputs, info.sizeArrInputs);
    waitTime(info.time);
  }
}

void readAnalogInputs(int * inputPins, int sInputPins){
  int * inputData = new int [sInputPins];
  for(int i=0; i<sInputPins; ++i){
    inputData[i] = analogRead(inputPins[i]);
  }
  printLikeJson(inputPins, sInputPins, inputData);
}

void printLikeJson(int * pins, int sizeArrs, int * data){
  Serial.print("{");
  for(int i=0; i<sizeArrs; ++i){
    Serial.print("\"");
    Serial.print(pins[i]);
    Serial.print("\":");
    Serial.print(data[i]);
    if(i+1==sizeArrs){ 
      continue;
    }else{
      Serial.print(",");
    }
  }
  Serial.println("}");
}

int jsonToArrayInt(char * json){
  //Create Json parser  
  JsonParser<20> parser;
  //Parse the json
  JsonObject root = parser.parse(json);
  //If error print error and stop until reset
  if (!root.success())
  {
    Serial.println("JsonParser.parse() failed");
    return 1;
  }
  
  long time = root["time"];
  time = time < MIN_TIME ? MIN_TIME : time; 
  
  JsonArray arr(root["data"]);
  
  //Store the json on the array arrinputs
  int count =0;
  int * arrInputs = new int[arr.size()];
  for (JsonArrayIterator i=arr.begin(); i!=arr.end(); ++i)
  { 
    int pin = atoi((char*)*i);
    //Omit the invelid pin
    if(pin<MIN_ANALOG_IN || pin>MAX_ANALOG_IN){
      continue;
    }
    arrInputs[count] = pin;
    ++count;
  }
  info.sizeArrInputs = arr.size();
  info.arrInputs = arrInputs;
  info.time = (int)time;
  info.isRead = 1;
  return 0;
}

//Default mode value 0 is INPUT
void setModeAnalogPin(int minPin, int maxPin, int mode){
  for(int pin=minPin; pin<maxPin; ++pin){
    pinMode(pin, mode);
  }
}

//wait n time without use delay
void waitTime(int wTime){
  initTime = millis()+wTime;
  while(millis()<initTime);
}
