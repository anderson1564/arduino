
//#include <SPI.h>                      //incluye la libreria SPI para la comunicación
//#include <SdFat.h>     
//#include <SdFatUtil.h> //incluye la libreria de la memoria
//#include <SFEMP3Shield.h>             //incluye la libreria del MP3
  //crea la variable para controlar las intrucciones del MP3
//Pins
#define AXIS_X A2
#define AXIS_Y A1
#define PIN_OUT 30
//Times
#define MAX_LOOP_TIME 300
#define FORCED_REDUCE_MAX 3  //Var to forced reduce the max angle
#define TURN_DELAY_TIME 60
//Vars accelerometer
#define POS_ZERO 340
#define MAX_DIFF 180
#define BUTTTON_INIT 22

//Init states
#define IS_STOP 0
#define IS_READY 1
#define IS_GO 2

typedef unsigned long long int uTime;
uTime dead_time=10000;
uTime dead_time2=dead_time;
//SdFat sd;
//SFEMP3Shield MP3player;
int init_state = IS_STOP;
void setup(){
  //Set the pin mode
  pinMode(PIN_OUT, OUTPUT);
  pinMode(BUTTTON_INIT, INPUT);
  //sd.begin(SD_SEL, SPI_HALF_SPEED);
  //MP3player.begin();
  //MP3player.setVolume(60);
}

//Process the the data of the axis
int procAxis(int axis){
  //Get the analog read
  int input = analogRead(axis);
  //Calc the new data base on the axis data
  int result = abs(POS_ZERO-input);
  //TODO: si esto funcuiona volver esto golbal y constante
  int max_angle = MAX_DIFF/FORCED_REDUCE_MAX;
  if(result>max_angle){
    result=max_angle;
  }
  return map(result, 0, max_angle, 0, MAX_LOOP_TIME/2);;
}

void axoDelay(unsigned long long int delay_time){
  //Loop based on the time
  uTime time_end = millis()+delay_time; //Final delay time
  while(millis() < time_end);
}

void blinkOutput(int delay_time){
  //Turn off the output
  //MP3player.stopTrack(); 
  digitalWrite(PIN_OUT, LOW);
  axoDelay(delay_time);
  //Turn on the output
  digitalWrite(PIN_OUT, HIGH);
  //MP3player.playTrack(1);
  axoDelay(delay_time);
}

void loop(){
  int in = digitalRead(BUTTTON_INIT);
  if(init_state!=IS_GO && in){
    if(init_state==IS_STOP){
      init_state=IS_READY;
    }else if(init_state==IS_READY){
      init_state=IS_GO;
      dead_time=millis()+60000;
    }
    axoDelay(300);
  }else if(init_state==IS_GO){
    //Turn off and turn on the output
    blinkOutput(TURN_DELAY_TIME);
    //Get the delay time
    uTime delay_time = MAX_LOOP_TIME-(procAxis(AXIS_X)+procAxis(AXIS_Y)-5);
    //Delay
    axoDelay(delay_time);
    if(millis()>=dead_time){
  //    MP3player.playTrack(002);
      axoDelay(5000);
  //    MP3player.stopTrack();
      init_state = IS_STOP;
    }
  }
}