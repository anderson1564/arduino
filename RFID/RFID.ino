/*
* Arduino UNO and MEGA
*   green wire: data 0, pin 2
*   White wire: data 1, pin 3
*/

#define SIZE_RFID 32

volatile long data = 0;
volatile int num_read = 0;

void setup() {
  Serial.begin(9600);
  attachInterrupt(0, getZero, FALLING);
  attachInterrupt(1, getOne, FALLING);
}

void getZero(){
  data <<=1;
  ++num_read;
}

void getOne(){
  data <<=1;
  ++data;
  ++num_read;
}

void loop() {
  if(num_read>=SIZE_RFID){
    Serial.println(data, BIN);
    num_read=0;
    analyzeData(data);
    data=0;
  }
}

void analyzeData(long data){

}

