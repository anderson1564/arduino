#include "MyServo.h"

/* 
 *  Recive two input, and set the servo pos, 
 *  based on the promedium of the inputs,
 *  return the setted position
*/
int MyServo::servoUpdate(int a, int b){
  pos = MyServo::calc_mov(analogRead(a), analogRead(b));
  set_servo(pos);
  return pos;
}

//Set the new servo pos
void MyServo::set_servo(int in){
  if(servo.read()!=in)
    servo.write(in);
}

//Cacl the servo position based on the inputs
int MyServo::calc_mov(int a, int b){
  return map(a+(1023-b) , 0, 2046, 0, 179);
}

//Contrutor, get and set the pin of the servo 
MyServo::MyServo(int pin){
  pos = 90;
  servo.attach(pin);
  set_servo(pos);
}

//Release the pin of the servo
MyServo::~MyServo(){
  if(servo.attached())
    servo.detach();
}

int MyServo::get_pos(){
  return pos;
}

/*
  Author: Anderson Lopez Monsalve
  Date: March 2014
*/
