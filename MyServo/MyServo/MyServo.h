#ifndef MYSERVO_h
#define MYSERVO_h

//Control import library version Arduino
#if ARDUINO >= 100
   #include <Arduino.h> // for Arduino 1.0 and later
#else
   #include <WProgram.h> // for earlier releases
#endif
#include <Servo.h>

class MyServo {
  int pos;
  Servo servo;
  
  void set_servo(int);
  int calc_mov(int, int);

  public: 
    MyServo(int);
    ~MyServo(void);
    int servoUpdate(int, int);
    int get_pos(void);
};

#endif
