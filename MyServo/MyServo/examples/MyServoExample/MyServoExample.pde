#include <Servo.h>
#include <MyServo.h>

//Servo 1
#define PIN_SERVO_1 10
#define SER_1_RES_1 9
#define SER_1_RES_2 8
//Servo 2
#define PIN_SERVO_2 11
#define SER_2_RES_1 7
#define SER_2_RES_2 6

//Init the servos
MyServo servo_1(PIN_SERVO_1);
MyServo servo_2(PIN_SERVO_2);

void setup(){
  Serial.begin(9600);
}

void loop(){
  //Update the servo position
  int pos_servo_1 = servo_1.servoUpdate(SER_1_RES_1, SER_1_RES_2);
  int pos_servo_2 = servo_2.servoUpdate(SER_2_RES_1, SER_2_RES_2);
  //Print the position
  Serial.print("Servo 1: ");
  Serial.println(pos_servo_1);
  Serial.print("Servo 2: ");
  Serial.println(pos_servo_2);
  delay(200);
}