#define fC 34
#define DEBUG 1

const byte motPins[] = {10,11,12,13};
const boolean sec[][4] = {{1,1,0,0},
                          {0,1,1,0},
                          {0,0,1,1},
                          {1,0,0,1}};
byte paso = 0;
long tini = 0;
long tstate = 0;

void setup()
{
  for(int i = 0; i < sizeof(motPins); i++){
    pinMode(motPins[i],OUTPUT);
    digitalWrite(motPins[i],LOW);
  }
  pinMode(fC,INPUT);
  Serial.begin(9600);
  tini = millis();
}

void giro(const unsigned long vel, const char sg='H')
{
  tstate = millis() - tini;
  for(int i = 0; i < sizeof(motPins); ++i){
     digitalWrite(motPins[i],sec[sg == 'H' ? paso : (sizeof(motPins)-paso-1)][i]);
  }

  if(tstate >= vel){
    ++paso%=4;
    tini = millis();
  }
}

void loop()
{
  while(digitalRead(fC)==LOW){
    giro(5);
    #if DEBUG
      Serial.print("giro: A tstate: ");
      Serial.print(tstate);
      Serial.print(" bobHIGH: ");
      Serial.print(motPins[3-paso]);
      Serial.print(" paso: ");
      Serial.println(paso);
    #endif
  }
}



