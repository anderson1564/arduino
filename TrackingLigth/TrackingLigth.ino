#include <Servo.h>

#define SERVO_PIN_1 9
#define SERVO_PIN_2 10
#define SER_1_IN_1 8
#define SER_1_IN_2 10
#define SER_2_IN_1 9
#define SER_2_IN_2 11

Servo servo_1;
Servo servo_2;

//Cacl the servo position based on the inputs
int servo_update(int a, int b){
  a = analogRead(a);
  b = analogRead(b);
  return map(a+(1023-b) , 0, 2046, 179, 0);
}

void setup(){
  Serial.begin(9600);
  servo_1.attach(SERVO_PIN_1);
  servo_2.attach(SERVO_PIN_2);  
}

void loop(){
  servo_1.write(servo_update(SER_1_IN_1, SER_1_IN_2));
  servo_2.write(servo_update(SER_2_IN_1, SER_2_IN_2));
  delay(200);
}
