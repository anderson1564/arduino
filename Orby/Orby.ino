#include <LiquidCrystal.h>

#define PIN_MOTOR 29
#define BUTTOM 28
//COUNT CONST
#define NUM_LEDS_COUNT 22
#define INIT_LED_COUNT 30
//COUNT STATES
#define OFF_ALL -1
#define ON 1
#define OFF 0
//CONST DARE
#define NUM_LEDS_DARE 3
#define INIT_LED_DARE 2
//PHASES
#define STOP 0
#define COUNT 1
#define MOTOR 2
#define DARE 3
#define COUNT_DARE 4

int phase = STOP;
//DARE Variables
const char* words[]={"CARRO", "HELADO", "EMBARAZO", "VERGUENZA", "ENAMORADO", "BOMBILLO", "TRABAJO", "GIMNASIO", "GAFAS", "ESCRIBIR", "CHATEAR", "PROFESOR", "OSO", "DESILUCIONADO", "CONSTRUIR", "DESTRUIR", "ELECTROCUTAR", "DESPECHO", "JUGAR", "MUNDIAL", "ARBITRO", "GRADUACIÓN", " ESTUDIANTE", "PAPA", "FLAMINGO", "IGLESIA", "MAQUILLAR", "PERSEGUIR", "ENFERMAR", "VIBRADOR", "CONVERSAR", "MENSAJERO", "PORTERO", "BRONCEARSE", "COCA-COLA", "ANGUSTIA", "RAPEAR", "MIMETIZAR", "DIBUJAR", "ENERGIA", "REGGAETON", "GORRA", "DESAYUNAR", "PIZZA", "CIEGO", "PORRISTA", "MESERA", "BAR", "DOCTOR", "TROMPETA", "SOL", "CELULAR", "GRITAR", "FUERZA", "MIMAR", "CALZON", "ROGAR", "VOLUMEN", "EMERGENCIA", "BOTON"};
const int NUM_WORDS = sizeof(words)/sizeof(char*);
LiquidCrystal lcd(22,23,24,25,26,27);
//COUNT Variables
const unsigned long CICLE_TIME = NUM_LEDS_COUNT*1000;
const unsigned long EXTRA_TIME = 3000;
unsigned long init_time;
unsigned long final_time;
unsigned long delay_time;
int num_press_buttom = 0;
int p_buttom_old;
int p_buttom;

//Init the pins
void init_pins(int init, int num, int mode){
  int sum = init+num;
  for(int i=init; i<sum; ++i){
    pinMode(i,mode);
  }
}

//Turn the leds until the num led, off the others
void turn_led_until(int last_led_on, int init, int num){
  int sum = init+num;
  for(int led=init; led<sum; ++led){
    digitalWrite(led ,led<=init+last_led_on ? ON : OFF); 
  }
}

//Print a word in the LCD
void controlLCD(int id){
  lcd.print(words[id]);
}

void cicle_crown_LEDs(int next_phase){
  int final_led_on = (final_time-init_time+(EXTRA_TIME*(num_press_buttom-2)))/1000;
  turn_led_until(final_led_on, INIT_LED_COUNT, NUM_LEDS_COUNT);
  //If is the final cicle
  if(final_led_on==0){
      orbyDelay(1000);
      turn_led_until(OFF_ALL, INIT_LED_COUNT, NUM_LEDS_COUNT);
      num_press_buttom = 0;
      phase=next_phase;
  }
}

//If the time with the extra times is over the cicle time, force then to be equal to the cicle time
void check_times(){
  int actual_total_time = (final_time-init_time+(EXTRA_TIME*(num_press_buttom-2)));
  if(actual_total_time>CICLE_TIME){
    num_press_buttom=2;
    init_time=millis();
    final_time= init_time+CICLE_TIME;
  }
}

void phase_count(){
  check_times();
  cicle_crown_LEDs(MOTOR);
}

void phase_motor(){
  //Init the motor
  digitalWrite(PIN_MOTOR, 1);
  orbyDelay(500);
  //Stop the motor
  digitalWrite(PIN_MOTOR, 0);
  //Change to nest phase
  phase=DARE;
}

void phase_dare(){
  //Set the values of the phase dare
  digitalWrite(INIT_LED_DARE+random(NUM_LEDS_DARE), HIGH);
  controlLCD(random(NUM_WORDS));
  phase=COUNT_DARE;
  num_press_buttom=1;
  //Update final time
  final_time=millis()+CICLE_TIME;  
}

void phase_count_dare(){
  cicle_crown_LEDs(STOP);
  if(phase==STOP){
    lcd.clear();
    turn_led_until(OFF_ALL, INIT_LED_DARE, NUM_LEDS_DARE);
  }
}

void orbyDelay(unsigned long delay_time){
  //Loop based on the time
  unsigned long time_end = millis()+delay_time; //Final delay time
  while(millis() < time_end);
}

void hadle_press_button(int p_buttom, int p_buttom_old){
  if(phase!=DARE && phase!=COUNT_DARE && p_buttom == 0 && p_buttom_old == 1){
    orbyDelay(500);
    if(phase==STOP or phase==COUNT){
      ++num_press_buttom;
    }
    if(num_press_buttom==1){
        phase=COUNT;
        final_time=millis()+CICLE_TIME;
        //Turn off all the leds of the phase DARE
        turn_led_until(OFF_ALL, INIT_LED_DARE, NUM_LEDS_DARE);
    }
  }
}

void loop(){
  init_time = millis();
  p_buttom = digitalRead(BUTTOM);
  hadle_press_button(p_buttom, p_buttom_old);
  switch(phase){
    case COUNT: phase_count();
                break;
    case MOTOR: phase_motor();
                break;
    case DARE:  phase_dare();
                break;
    case COUNT_DARE:  phase_count_dare();
                      break;
  }
  //Refresh the state of the button
  p_buttom_old = p_buttom;
}

void setup(){
  //Set the seed of the random
  randomSeed(analogRead(0));
  //Set the pin mode
  pinMode(BUTTOM, INPUT);
  pinMode(PIN_MOTOR, OUTPUT);
  init_pins(INIT_LED_COUNT, NUM_LEDS_COUNT, OUTPUT);
  init_pins(INIT_LED_DARE, NUM_LEDS_DARE, OUTPUT);
  //Set the LCD
  lcd.begin(16, 2);
  //Turn off all the LEDs
  turn_led_until(OFF_ALL, INIT_LED_COUNT, NUM_LEDS_COUNT);
  turn_led_until(OFF_ALL, INIT_LED_DARE, NUM_LEDS_DARE);
}
