#define DEBUG 1

#define CICLE_ALARM_TIME 200l

#define LED 7
#define BUZZER 13
//Echo -> Input
#define U_SOUND_IN 8
//Trig -> Output
#define U_SOUND_OUT 4
#define WEIGHT A0

//HC-SR04 -> 200 cm, 5 more for safe
#define MAX_DIST_SENSOR 205
#define NUM_SAMPLES 10
#define COUNT_TIME 10000l
#define LIMIT_WEIGHT 800

//Distance
long int end_time = 0;
int distance_border = 0;
boolean is_counting = false;
//Alarm
long int cicle_alarm_end_time=0;
int cicle_alarm=HIGH;

void setup() {
  #ifdef DEBUG
    Serial.begin(9600);
  #endif
  //Init the led
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  //Init the buzzer
  pinMode(BUZZER, OUTPUT);
  digitalWrite(BUZZER, LOW);
  //Init the ultra sound
  pinMode(U_SOUND_IN, INPUT);
  pinMode(U_SOUND_OUT, OUTPUT);
  //Set the distance to the border
  #ifdef DEBUG
    Serial.println("Getting distance border ...");
  #endif
  distance_border = get_fix_distance();
  #ifdef DEBUG
    Serial.print("Distance border: ");
    Serial.println(distance_border);
  #endif
}

void blink_led(){
  digitalWrite(LED, HIGH);   
  delay(250);               
  digitalWrite(LED, LOW);    
  delay(20);
}

//Get the distance on cm
int get_distance(){
  //Stabilized sensor
  digitalWrite(U_SOUND_OUT, LOW);
  delayMicroseconds(5);
  //Send ultra sonic pulse
  digitalWrite(U_SOUND_OUT, HIGH);
  delayMicroseconds(10);
  //Get the buncing sound time and multiply by the speed
  //d =v*t (on cm)
  return int(0.017 * pulseIn(U_SOUND_IN, HIGH));
}

//Get the distance to the border
int get_fix_distance(){
  //set a high invalid distance
  int min_read = MAX_DIST_SENSOR + 1;
  for(int i=0, sample=0; i<NUM_SAMPLES; ++i){
    sample = get_distance();
    #ifdef DEBUG
      Serial.print("border: ");
      Serial.println(sample);
    #endif
    if(sample<min_read){
      min_read = sample;
    }
    //TODO: esto se puede mejorar
    blink_led();
    delay(100);
    
    //if in the last lecture the min read is invalid reset the cicle
    if(i==(NUM_SAMPLES-1) && 
      (min_read<0 || min_read>MAX_DIST_SENSOR)){
      i=0;
      min_read = MAX_DIST_SENSOR + 1;
      #ifdef DEBUG
      Serial.println("Reset sample ...");
    #endif
    }
  }
  return min_read;
}

boolean check_distance(){
  /*
    Note of the programmer:
      Set actual_time can sacrifice pression in some milliseconds,
      but make the code more readable, 
      this is not a nuclear operative system anyway XD
  */
  long int actual_time = millis();
  int distance = get_distance();
  if(distance < distance_border){
    if(is_counting==false){
      //Set the initial values of the count state
      end_time = actual_time + COUNT_TIME;
      is_counting = true;
    }
  }else if(is_counting){
      is_counting=false;
  }
  boolean is_distance = is_counting && (actual_time>end_time);
  #ifdef DEBUG
    Serial.print(" Distance: ");
    Serial.print(distance);
    Serial.print(" ");
    Serial.print(is_distance);
    Serial.print(" ");
  #endif
  return is_distance;
}

boolean is_counting_weight=false;
long int end_time_weight = 0;

boolean check_weight(){
  int weight = analogRead(WEIGHT);
  boolean is_weight = weight > LIMIT_WEIGHT;
  
  long int actual_time = millis();

  if(weight > LIMIT_WEIGHT){
    if(is_counting_weight==false){
      //Set the initial values of the count state
      end_time_weight = actual_time + COUNT_TIME;
      is_counting_weight = true;
    }
  }else if(is_counting_weight){
      is_counting_weight=false;
  }
  is_weight = is_counting_weight && (actual_time>end_time_weight);
  
  #ifdef DEBUG
    Serial.print(" Weight: ");
    Serial.print(weight);
    Serial.print(" ");
    Serial.print(is_weight);
    Serial.print(" ");
  #endif
  return is_weight;
}

void alarm(boolean state){
  if(state){
    long actual_time=millis();
    //Check if update the cicle o fthe alarm
    if(actual_time>cicle_alarm_end_time){
      //Update the state of the alarm cicle
      cicle_alarm_end_time = actual_time + CICLE_ALARM_TIME;
      cicle_alarm= cicle_alarm==HIGH ? LOW : HIGH;
    }
  }else{
    //set down the alarm cicle (turn off the alarm)
    cicle_alarm= LOW;
  }
  //Set the virtual states to the real
  digitalWrite(BUZZER, cicle_alarm);
  digitalWrite(LED, cicle_alarm);
}

void loop() {
  boolean is_alarm = check_weight() || check_distance();
  alarm(is_alarm);
  #ifdef DEBUG
    Serial.println();
  #endif
}