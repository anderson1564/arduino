#include <Servo.h>
#define PIN_SERVO_1 10
#define PIN_SERVO_2 11

#define PIN_LIGTH_1 2 //s1
#define PIN_LIGTH_2 4 //s2
#define PIN_LIGTH_3 3 //s3
#define PIN_LIGTH_4 5 //s4

Servo servo1;
Servo servo2;

int calcServoPos(int a, int b){
  return map(a+(1023-b) , 0, 2046, 0, 179);
}

void setup(){
  Serial.begin(9400);
  servo1.attach(PIN_SERVO_1);
  servo2.attach(PIN_SERVO_2);
}

void loop(){
  int ar1 = analogRead(PIN_LIGTH_1);
  int ar2 = analogRead(PIN_LIGTH_2);
  int ar3 = analogRead(PIN_LIGTH_3);
  int ar4 = analogRead(PIN_LIGTH_4);
  
  int pos1 = calcServoPos(ar1, ar2);
  int pos2 = calcServoPos(ar3, ar4);
  
  servo1.write(pos1);
  servo2.write(pos2);
  Serial.print("             ");
  delay(10);
  Serial.print(ar1);
  delay(10);
  Serial.print("             ");
  delay(10);
  Serial.print(ar2);
  delay(10);
  Serial.print("             ");
  delay(10);
  Serial.print(ar3);
  delay(10);
  Serial.print("             ");
  delay(10);
  Serial.print(ar4);
  delay(10);
  Serial.println();



 /* Serial.print(" P1:");
  Serial.print(pos1);
  Serial.print(" P2: ");
  Serial.println(pos2);
  */
  delay(30);
}
